#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include "gsl/gsl_poly.h"
#include "/Users/aaa/myfile/github/c99lib/AronCLibNew.h"


// #include "/usr/local/Cellar/gsl/2.7.1/include/gsl/gsl_poly.h"
// SEE: Users/aaa/myfile/bitbucket/stackproject/haskellffi
 
int increment(int n) {
  return n + 1;
}

void print_ascii(){
	for(int i = 32; i < 127; i++){
	  printf("%d 0x%x %c\n", i, i, i);
	}
}

void addOne(int* n){
    int m = *n;
    *n = m + 1;
}

// KEY: allocate and free memory in Haskell, set value in C function
void alloca_memory(int* pt, int len){
  for(int i = 0; i < len; i++){
    pt[i] = i;
  }
}

double dotn(int n, const double* a0, const double* a1){ 
  double s = 0.0;
  for(int i = 0; i < n; i++){
    s += a0[i] * a1[i];
  }
  return s;
}

void addVec(int n, double* a0, double* a1, double* ret){ 
  for(int i = 0; i < n; i++){
    ret[i] = a0[i] + a1[i];
  }
}

void subVec3(double a0[3], double a1[3], double ret[3]){ 
  ret[0] = a0[0] - a1[0];
  ret[1] = a0[1] - a1[1];
  ret[2] = a0[2] - a1[2];
}

void mulScaleVec3(double x, double a[3], double ret[3]){ 
  ret[0] = x * a[0];
  ret[1] = x * a[1];
  ret[2] = x * a[2];
}

void mulVec3Scale(double a[3], double x, double ret[3]){ 
  mulScaleVec3(x, a, ret);
}

/*
 *  KEY: matrix multiply vector
 *  NOTE: column matrix x column vector
 *  m = 2
 *  n = 3
 *  a[6] = {1, 2, 3, 4, 6, 7};
 *  =>
 *  1 2 3
 *  4 6 7 
 *  mxn, matrix x vector
   {
      int m = 2;
      int n = 2;
      double a[4] = {1, 2, 3, 4};
      double v[2] = {1, 2};
      double ret[2];
      multiVec(m, n, a, v, ret);
      for(int i = 0; i < m; i++){
        printf("%f\n", ret[i]);
      }
   }
 */
void multiVec(int m, int n, const double* a, 
              const double* v, 
              double* const ret){
  for(int i = 0; i < m; i++){
    double d = 0.0;
    for(int j = 0; j < n; j++){
      d += a[n * i + j] * v[j];
    }
    ret[i] = d;
  }
}

void multiVec2(int m, int n, const double* a, 
               const double* v, 
               double* ret){
  for(int i = 0; i < m; i++){
    ret[i] = dotn(n, a + n*i, v); 
  }
}


/*
 *   2 x 3
 *
 *   1 2 3
 *   4 5 6
 *
 *   3 x 4
 *
 *   1 2 3 4
 *   5 6 7 8
 *   9 9 9 9
 *
 *
 */
void multiMat2(int m0, int n0, const double* a,
               int m1, int n1, const double* b, 
               double* const ret) {
}


/*
     //
     //  1 2 3
     //  4 5 6
     //
     //  1 2
     //  3 4
     //  5 6
     //
     //  1 * 1 + 2 * 3 + 3 * 5 = 22
     //  4 * 1 + 5 * 3 + 6 * 5 = 49
     //
     //  1 * 2 + 2 * 4 + 3 * 6 = 28
     //  4 * 2 + 5 * 4 + 6 * 6 = 64
     //
     //  22 28
     //  49 64
     //
      double a[6] = {1, 2, 3, 4, 5, 6};
      double b[6] = {1, 2, 3, 4, 5, 6};
      double ret[4];
      int nRow = 2;
      int nCol = 3;
      multiMat(nRow, nCol, a, nCol, nRow, b, ret);
*/
void multiMat(int m, int n, const double* a, 
              int n1, int h, const double* b, 
              double* const ret){
  for(int k = 0; k < h; k++){
    for(int i = 0; i < m; i++){

      // dot product
      double d = 0.0;
      for(int j = 0; j < n; j++){
        d += a[i * n + j] * b [j * h + k];
      }

      ret[i*h + k]  = d;
    }
  }
}

int inx(int i, int j, int w){
  return i * w + j;
}


/*
 * KEY: generate random int array from min to max in length len
 *
     int arr[4];
     int len = 4;
     int min = 0;
     int max = 2;
     randomArray(len, min, max, arr); 
     for(int i = 0; i < len; i++){
       printf("%d ", arr[i]);
     }
 *
 */
void randomArray(long len, int min, int max, int* arr){
  srand(time(NULL));
  for(long i = 0; i < len; i++){
    int n = (rand() % (max - min + 1)) + min;
    arr[i] = n;
  }
}


/* Solve for real or complex roots of the standard quadratic equation,
 * returning the number of real roots.
 *
 * SEE: /usr/local/Cellar/gsl/2.7.1/include/gsl/gsl_poly.h
 * Roots are returned ordered.
 */
/*
int gsl_poly_solve_quadratic_x (double a, double b, double c, 
                              double * x0, double * x1){
   return gsl_poly_solve_quadratic (a, b, c, x0, x1);
}
*/


// #define OK
#ifdef OK 
int main(){
  {
    double a0[3] = {1, 2, 3};
    double a1[3] = {2, 3, 4};
    double b = dotn(3, a0, a1);
    printf("b=%f\n", b);
  }
   {
     //  f(x) = (x - 1) * (x - 2) 
     //  f(x) = 0
     //  => x0 = 1, x1 = 2
     // double a = 1;
     // double b = -3;
     // double c = 2;
     // double x0;
     // double x1;
     // int ret = gsl_poly_solve_quadratic_x(a, b, c, &x0, &x1);
     // printf("x0=%f x1=%f\n", x0, x1);
     // printf("ret=%d\n", ret);
   }
   {
     //
     //  1 2   1
     //  3 4   2
     //  1 * 1 + 2 * 2 = 5
     //  3 * 1 + 4 * 2 = 11
     //
      int m = 2;
      int n = 2;
      double a[4] = {1, 2, 3, 4};
      double v[2] = {1, 2};
      double ret[2];
      multiVec(m, n, a, v, ret);
      for(int i = 0; i < m; i++){
        printf("%f\n", ret[i]);
      }
      double expectedArr[2] = {5, 11};

      t_doublePt(2, ret, 2, expectedArr, "multiVec 1");
   }
   {
     //
     // 1 2 3  1  
     // 4 5 6  2
     //        3
     //
     // 1 * 1 + 2 * 2 + 3 * 3 = 14
     // 4 * 1 + 5 * 2 + 6 * 3 = 32
     //
      const int m = 2;
      const int n = 3;
      double a[6] = {1, 2, 3, 4, 5, 6};
      double v[n] = {1, 2, 3};
      double ret[m];
      multiVec(m, n, a, v, ret);
      for(int i = 0; i < m; i++){
        printf("%f\n", ret[i]);
      }
      double expectedArr[2] = {14, 32};

      t_doublePt(2, ret, 2, expectedArr, "multiVec 2");
   }
   {
      double a[4] = {1, 2, 3, 4};
      double v[4] = {1, 2, 3, 4};
      t_doublePt(4, a, 4, v, "double check");
   }
   {
      double a[4] = {1, 2, 3, 4};
      double v[4] = {1, 2, 3, 4};
      t_doublePt(4, a, 4, v, "double check ok");
   }

   {
     //
     //  1 2 3
     //  4 5 6
     //
     //  1 2
     //  3 4
     //  5 6
     //
     //  1 * 1 + 2 * 3 + 3 * 5 = 22
     //  4 * 1 + 5 * 3 + 6 * 5 = 49
     //
     //  1 * 2 + 2 * 4 + 3 * 6 = 28
     //  4 * 2 + 5 * 4 + 6 * 6 = 64
     //
     //  22 28
     //  49 64
     //
      double a[6] = {1, 2, 3, 4, 5, 6};
      double b[6] = {1, 2, 3, 4, 5, 6};
      double ret[4];
      int m = 2;
      int n = 3;
      multiMat(m, n, a, n, m, b, ret);
      printArrayFixedD(ret, 4, 2);
      printf("matrix a\n");
      printArrayFixedD(a, 6, 3);
      printf("matrix b\n");
      printArrayFixedD(b, 6, 2);
      double expArr[4] = {22, 28, 49, 64};
      t_doublePt(4, ret, 4, expArr, "multiMat 1");
   }
   {
     //
     //  1 2 3
     //  4 5 6
     //
     //  1 2
     //  3 4
     //  5 6
     //
     //  1 * 1 + 2 * 3 + 3 * 5 = 22
     //  4 * 1 + 5 * 3 + 6 * 5 = 49
     //
     //  1 * 2 + 2 * 4 + 3 * 6 = 28
     //  4 * 2 + 5 * 4 + 6 * 6 = 64
     //
     //  22 28
     //  49 64
     //
      double a[6] = {1, 2, 3, 4, 5, 6};
      double b[6] = {2, 3, 4, 5, 6, 7};
      double ret[4];
      int m = 2;
      int n = 3;
      multiMat(m, n, a, n, m, b, ret);
      printArrayFixedD(ret, 4, 2);
      double expArr[4] = {28, 34, 64, 79};
      t_doublePt(4, ret, 4, expArr, "multiMat 2");
   }
   {
      double a[6] = {1, 2, 3, 4, 5, 6};
      double b[3] = {2, 3, 4};
      double ret[2];
      int m = 2;
      int n = 3;
      multiVec2(m, n, a, b, ret);
      printArrayFixedD(ret, 2, 2);
      double expArr[4] = {28, 34};
      t_doublePt(2, ret, 2, expArr, "multiMat 3");
   }
   {
     int x0 = 0;
     int x1 = 0;
     int arr[1] = {1};
     int piv = partition(x0, x1, arr);
     t_int(piv, 0, "partition 1");
   }
   {
     int x0 = 0;
     int x1 = 1;
     int arr[2] = {1, 2};
     int piv = partition(x0, x1, arr);
     t_int(piv, 1, "partition 2");
   }
   {
     int x0 = 0;
     int x1 = 1;
     int arr[2] = {2, 1};
     int piv = partition(x0, x1, arr);
     t_int(piv, 0, "partition 3");
   }
   {
     /*
      *   x
      *   2 1 3
      *   x 
      *     x
      *   2 1 3
      *     x
      *       x
      *   2 1 3
      *       x
      */
     int x0 = 0;
     int x1 = 2;
     int arr[3] = {2, 1, 3};
     int piv = partition(x0, x1, arr);
     t_int(piv, 2, "partition 4");
   }
   {
     int x0 = 0;
     int x1 = 2;
     int arr[3] = {1, 2, 3};
     int piv = partition(x0, x1, arr);
     t_int(piv, 2, "partition 5");
   }
   {
     /*
      *  x
      *  3 2 1
      *  x
      *
      *  x
      *  3 2 1
      *    x
      *
      *  x
      *  3 2 1
      *      x
      *
      *  x
      *  1 2 3
      *        x
      *
      */
     int x0 = 0;
     int x1 = 2;
     int arr[3] = {3, 2, 1};
     int piv = partition(x0, x1, arr);
     t_int(piv, 0, "partition 6");
   }
   {
     int x0 = 0;
     int x1 = 0;
     int len1 = x1 - x0 + 1;
     int arr[1] = {1};
     quickSortInt(x0, x1, arr);
     int len2 = 1;
     int expArr[1] = {1};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 1");
   }
   {
     int x0 = 0;
     int x1 = 1;
     int len1 = x1 - x0 + 1; 
     const int len2 = 2;
     int arr[2] = {1, 2};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {1, 2};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 2");
   }
   {
     unsigned long x0 = 0;
     unsigned long x1 = 1;
     unsigned long len1 = x1 - x0 + 1; 
     const unsigned long len2 = 2;
     int arr[len2] = {2, 1};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {1, 2};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 3");
   }
   {
     int x0 = 0;
     int x1 = 2;
     int len1 = x1 - x0 + 1; 
     const int len2 = 3;
     int arr[3] = {2, 1, 3};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {1, 2, 3};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 4");
   }
   {
     int x0 = 0;
     int x1 = 3;
     int len1 = x1 - x0 + 1; 
     const int len2 = 4;
     int arr[4] = {2, 1, 3, 0};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {0, 1, 2, 3};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 5");
   }
   {
     int x0 = 0;
     int x1 = 3;
     int len1 = x1 - x0 + 1; 
     const int len2 = 4;
     int arr[4] = {0, 1, 2, 3};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {0, 1, 2, 3};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 6");
   }
   {
     int x0 = 0;
     int x1 = 3;
     int len1 = x1 - x0 + 1; 
     const int len2 = 4;
     int arr[4] = {3, 2, 1, 0};
     quickSortInt(x0, x1, arr);
     int expArr[len2] = {0, 1, 2, 3};
     t_intPtr(len1, arr, len2, expArr, "quickSortInt 7");
   }
   {
     int arr[4];
     int len = 4;
     int min = 0;
     int max = 2;
     randomArray(len, min, max, arr); 
     for(int i = 0; i < len; i++){
       printf("%d ", arr[i]);
     }
   }
   
   {

       fw(""); 
       const int len = 10000; 
       int lo = 0;
       int hi = len - 1;
       int arr[len];
       int min = 0;
       int max = 1000;
       randomArray(len, min, max, arr);
       quick_sort(arr, lo, hi);
    }
   {

       fw(""); 
       const int len = 100000; 
       int lo = 0;
       int hi = len - 1;
       int arr[len];
       // int min = 0;
       // int max = 1000;
       // randomArray(len, min, max, arr);
       // quick_sort(arr, lo, hi);
    }
    {
     fw(""); 
     const long len = 1E7; 
     long lo = 0;
     long hi = len - 1;
     int *arr = (int*)malloc(sizeof(int) * len);
     long min = 1;
     long max = 1000;
     randomArray(len, min, max, arr);
      clock_t start = clock();

      unsigned long old = (unsigned long)time(NULL);

      long long t_old = time_nano();

      quickSortInt(lo, hi, arr);

      clock_t end = clock() ;
      
      long long t_new = time_nano();

      double elapsed_time = (end-start)/(double)CLOCKS_PER_SEC; 

      unsigned long curr = (unsigned long)time(NULL);
      
      unsigned long diff = curr - old;
      unsigned long t_diff = t_new - t_old;
      fw("quickSortInt");
      printf("old time=%lu curr time=%lu diff=%lu\n", old, curr, diff);
      printf("elapsed_time=%f\n", elapsed_time);
      printf("t_time=%lld\n", t_diff);
      free(arr);
      // printArrayint(arr, len);
   }
   
   return 0;
}
#endif

