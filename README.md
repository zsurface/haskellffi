# haskellffi

* Build object file for $sp/haskellffi/src/Main.hs
* NOTE: Comment out the `main` in AronCLibFFI.c

Compile C to object file
```
gcc -c -o AronCLibFFI.o AronCLibFFI.c
```

* '$sp/haskellffi' Cabal file

```
/Users/aaa/myfile/bitbucket/stackproject/haskellffi/haskellffi.cabal

c-sources:           /Users/aaa/myfile/bitbucket/haskellffi/AronCLibFFI.c
```
